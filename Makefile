# MIT License
# Copyright (c) 2017-2024 Nicola Worthington <nicolaw@tfb.net>
# https://gitlab.com/nicolaw/trinitycore

.PHONY: test build mapdata clean run tdb sql pull getmapdata
.PHONY: build-all pull-all publish-all build-npcbots run-npcbots
.PHONY: release-all
.DEFAULT_GOAL := help

# Variant image to build. Defaults to "sql" for easy use with bundled
# docker-compose.yaml.
#VARIANT = slim
#VARIANT = npcbots
export VARIANT = sql

DOCKERFILE = Dockerfile
DOCKER_BUILDX_PLATFORMS ?= 
ifneq ($(strip $(DOCKER_BUILDX_PLATFORMS)),)
  DOCKER_BUILD_CMD ?= docker buildx build --push --platform $(DOCKER_BUILDX_PLATFORMS)
else
  DOCKER_BUILD_CMD ?= docker build
endif

# Where this Makefile resides.
MAKEFILE = $(firstword $(MAKEFILE_LIST))
MAKEFILE_DIR = $(shell cd $(dir $(MAKEFILE)) && pwd)

# What realm ID and port should the worldserver identify itself as.
WORLDSERVER_REALM_ID = 1
WORLDSERVER_PORT = 8085
WORLDSERVER_IP = $(shell ipconfig getifaddr en0 || hostname -I | egrep -o '[0-9\.]{7,15}' | grep -v ^127. | head -1)
#WORLDSERVER_NAME = $(shell hostname -s | sed -e "s/\b\(.\)/\u\1/g")
WORLDSERVER_NAME = $(shell awk 'BEGIN {srand()} {line[NR] = $$0} END {print line[int(NR * rand()) + 1]}' realm-names.txt)
WORLDSERVER_CONF = worldserver.conf
ifeq ($(VARIANT),npcbots)
WORLDSERVER_CONF = worldserver-$(VARIANT).conf
endif
export WORLDSERVER_CONF

# Container label information.
VCS_REF = $(shell git rev-parse HEAD)
BUILD_DATE = $(shell date +'%Y-%m-%d %H:%M:%S%z')
BUILD_VERSION = $(shell head -n 1 VERSION)

# Where to pull the upstream TrinityCore source from.
GITHUB_API = https://api.github.com
TC_GITHUB_REPO = TrinityCore/TrinityCore
TC_GIT_REPO = https://github.com/$(TC_GITHUB_REPO).git
TC_GIT_BRANCH = 3.3.5
TC_VCS_REF := $(shell git ls-remote "$(TC_GIT_REPO)" "$(TC_GIT_BRANCH)" | cut -f1)

# Where to pull optional Trinity-Bots source from.
TRINITY_BOTS_REPO = https://github.com/trickerer/Trinity-Bots

# What to call the resulting container image.
IMAGE_TAG_PREFIX ?=
ifeq ($(strip $(TC_GIT_BRANCH)),3.3.5)
ifeq ($(strip $(VARIANT)),sql)
IMAGE_TAG ?= $(TC_GIT_BRANCH)-$(VARIANT) $(TC_GIT_BRANCH) $(VARIANT)
else
IMAGE_TAG ?= $(TC_GIT_BRANCH)-$(VARIANT) $(VARIANT)
endif
else
ifeq ($(strip $(VARIANT)),sql)
IMAGE_TAG ?= $(TC_GIT_BRANCH)-$(VARIANT) $(TC_GIT_BRANCH)
else
IMAGE_TAG ?= $(TC_GIT_BRANCH)-$(VARIANT)
endif
endif
EXTRA_TAGS ?=
CI_REGISTRY ?= docker.io
IMAGE_REGISTRY = $(CI_REGISTRY)
CI_REGISTRY_IMAGE ?= $(IMAGE_REGISTRY)/nicolaw/trinitycore
IMAGE_REPO = ${CI_REGISTRY_IMAGE}
IMAGE_NAMES = $(addprefix $(IMAGE_REPO):,$(addprefix $(IMAGE_TAG_PREFIX),$(IMAGE_TAG))) $(addprefix $(IMAGE_REPO):,$(addprefix $(IMAGE_TAG_PREFIX),$(EXTRA_TAGS)))
IMAGE_NAME = $(word 1,$(IMAGE_NAMES))

# Try to find docker-compose command
DOCKER_COMPOSE_FILE = docker-compose.yaml
ifneq ($(shell which docker-compose 2>/dev/null),)
DOCKER_COMPOSE_CMD := docker-compose
else ifeq ($(shell docker compose version >/dev/null 2>&1; echo $$?), 0)
DOCKER_COMPOSE_CMD := docker compose
else
$(error Neither docker-compose nor docker compose found in PATH)
endif

# SHA of upstream TrinityCore source that has been built (used for publishing).
IMAGE_TC_VCS_REF = $(shell docker run --rm $(IMAGE_NAME) head -n 1 /.git-rev-short)

# Full database dump for the worldserver to populate the "world" database table with.
# https://github.com/TrinityCore/TrinityCore/releases
TDB_FULL_URL = $(shell $(MAKEFILE_DIR)/gettdb -q -q -n "$(TC_VCS_REF)")
TDB_7ZIP_FILE = $(notdir $(TDB_FULL_URL))
TDB_SQL_FILE = $(patsubst %.7z,%.sql,$(TDB_7ZIP_FILE))

# Location of WoW game client files, used to generate worldserver map data.
GAME_CLIENT_DIR = ./World_of_Warcraft/

# Directories expected to be generated for worldserver map data.
MAP_DATA_DIR = ./mapdata/

# Published pre-generated worldserver map data archive.
MAPDATA_FULL_URL = $(shell $(MAKEFILE_DIR)/getmapdata -q -q -n "$(TC_GIT_BRANCH)")

# MPQ game data files use to generate the worldserver map data.
MPQ_LOCALE = $(notdir $(shell find $(GAME_CLIENT_DIR)Data/ -mindepth 1 -maxdepth 1 -type d 2>/dev/null || echo enUS))
MPQ = $(addprefix $(GAME_CLIENT_DIR)Data/, $(addsuffix .MPQ, \
	common expansion patch-3 patch-2 patch common-2 lichking \
	$(MPQ_LOCALE)/lichking-speech-$(MPQ_LOCALE) \
	$(MPQ_LOCALE)/expansion-speech-$(MPQ_LOCALE) \
	$(MPQ_LOCALE)/lichking-locale-$(MPQ_LOCALE) \
	$(MPQ_LOCALE)/expansion-locale-$(MPQ_LOCALE) \
	$(MPQ_LOCALE)/base-$(MPQ_LOCALE) \
	$(MPQ_LOCALE)/patch-$(MPQ_LOCALE)-2 \
	$(MPQ_LOCALE)/backup-$(MPQ_LOCALE) \
	$(MPQ_LOCALE)/speech-$(MPQ_LOCALE) \
	$(MPQ_LOCALE)/patch-$(MPQ_LOCALE)-3 \
	$(MPQ_LOCALE)/patch-$(MPQ_LOCALE) \
	$(MPQ_LOCALE)/locale-$(MPQ_LOCALE) ) )

.INTERMEDIATE: $(MPQ)

sql:
	$(MAKEFILE_DIR)/getsql "$(TC_GIT_BRANCH)"

tdb: $(TDB_SQL_FILE)

$(TDB_SQL_FILE): $(TDB_7ZIP_FILE)
	test -s "$@" || { cd $(MAKEFILE_DIR) && 7zr x -y -- "$<"; }
	test -s "$@" && touch "$@"

$(TDB_7ZIP_FILE):
	$(MAKEFILE_DIR)/gettdb "$(TC_GIT_BRANCH)"

help:
	@echo ""
	@echo "Refer to the following documents for additional help:"
	@echo "  https://gitlab.com/nicolaw/trinitycore/-/blob/main/README.md"
	@echo "  https://gitlab.com/nicolaw/trinitycore/-/blob/main/GettingStarted.md"
	@echo "  https://gitlab.com/nicolaw/trinitycore/-/blob/main/Building.md"
	@echo "  https://www.youtube.com/playlist?list=PLb_n3pHJuv5ZRI2QkdpnSS-2dPy6UPzSl"
	@echo "  https://www.trinitycore.org/"
	@echo "  https://trinitycore.info/"
	@echo "  https://trinitycore.net/"
	@echo "  https://trinitycore.atlassian.net/wiki/spaces/tc/"
	@echo "  https://github.com/trickerer/Trinity-Bots/ (alternative build)"
	@echo ""
	@echo "Settings:"
	@echo "  VARIANT=$(VARIANT) # Valid values are 'sql', 'slim' and 'npcbots'"
	@echo "  TC_GIT_BRANCH=$(TC_GIT_BRANCH) # Git branch from $(TC_GIT_REPO) to build"
	@echo "  TC_GIT_REPO=$(TC_GIT_REPO) # Git repository to build from"
	@echo "  TC_VCS_REF=$(TC_VCS_REF) # Specific Git commit reference to build from"
	@echo "  IMAGE_REPO=$(IMAGE_REPO)"
	@echo "  IMAGE_NAMES=$(IMAGE_NAMES)"
	@echo "  TDB_FULL_URL=$(TDB_FULL_URL)"
	@echo "  MAPDATA_FULL_URL=$(MAPDATA_FULL_URL)"
	@echo ""
	@echo "Building for different TrinityCore branches:"
	@echo "  make build TC_GIT_BRANCH=3.3.5"
	@echo "  make build TC_GIT_BRANCH=cata_classic"
	@echo "  make build TC_GIT_BRANCH=master"
	@echo ""
	@echo "Use 'make build' to build the TrinityCore container image."
	@echo "Use 'make build-npcbots' to build the TrinityCore container image with Trinity-Bots (NPCBots) patch. (Alternative)"
	@echo "Use 'make pull' to download the TrinityCore container image instead of building it."
	@echo "Use 'make shell' to launch an interactive shell inside the TrinityCore container."
	@echo "Use 'make mapdata' to generate worldserver map data from the WoW game client."
	@echo "Use 'make getmapdata' to download pre-generated worldserver map data. (Alternative)"
	@echo "Use 'make run' to launch the TrinityCore servers using docker-compose."
	@echo "Use 'make run-npcbots' to launch TrinityCore servers with the Trinity-Bots (NPCBots) patch using docker-compose."
	@echo "Use 'make tdb' to download the full worldserver database SQL dump. (Optional)"
	@echo "Use 'make sql' to download the SQL files omitted in the 'slim' container. (Optional)"
	@echo "Use 'make dumpdb' to create an SQL dump backup of the dataases."
	@echo "Use 'make test' to test the TrinityCore servers start using docker-compose."
	@echo "Use 'make clean' to destroy ALL container images and MySQL database volume."
	@echo ""

clean:
	@while [ -z "$$CONFIRM_CLEAN" ]; do \
		read -r -p "Are you sure you want to delete ALL built and intermediate container images and database volumes? [y/N]: " CONFIRM_CLEAN; \
	done; [ "$$CONFIRM_CLEAN" = "y" ]
	$(DOCKER_COMPOSE_CMD) -f $(DOCKER_COMPOSE_FILE) down || true
	$(DOCKER_COMPOSE_CMD) -f $(DOCKER_COMPOSE_FILE) rm -sfv || true
	docker volume rm -f trinitycore_db-data trinitycore_logs || true
	docker image rm -f $(shell docker image ls --filter "label=org.opencontainers.image.title=nicolaw/trinitycore-intermediate-build" --quiet) || true
	docker image rm -f $(shell docker image ls --filter "label=org.opencontainers.image.title=nicolaw/trinitycore" --quiet) || true
	$(RM) .tdb-full-url.* .mapdata-full-url.* mysql.log worldserver.log authserver.log sql/custom/auth/0002-fix-realmlist.sql

run-npcbots:
ifeq ($(TC_GIT_BRANCH),3.3.5)
	$(MAKE) -f $(MAKEFILE) run VARIANT=npcbots WORLDSERVER_CONF=worldserver-npcbots.conf
else
	@echo "Error: TC_GIT_BRANCH '$(TC_GIT_BRANCH)' used with target '$@' - Trinity-Bots (NPCBots) is only compatible with the TC_GIT_BRANCH '3.3.5' branch of TrinityCore; aborting!"
	exit 1
endif

ifeq ($(TC_GIT_BRANCH),3.3.5)
run: $(MAP_DATA_DIR)mmaps sql/custom/auth/0002-fix-realmlist.sql
	WORLDSERVER_CONF="$(WORLDSERVER_CONF)" VARIANT="$(VARIANT)" $(DOCKER_COMPOSE_CMD) -f $(DOCKER_COMPOSE_FILE) up
else
run:
	@echo "Error: TC_GIT_BRANCH '$(TC_GIT_BRANCH)' used with target '$@' - $(DOCKER_COMPOSE_FILE) is only compatible with the TC_GIT_BRANCH '3.3.5' branch of TrinityCore; aborting!"
	exit 1
endif

# Create custom SQL file to be imported on first run that updates the IP
# address of the worldserver to be something other than just localhost.
# https://trinitycore.atlassian.net/wiki/spaces/tc/pages/2130016/realmlist
sql/custom/auth/0002-fix-realmlist.sql:
	printf 'REPLACE INTO realmlist (id,name,address,port) VALUES ("%s","%s","%s","%s");\n' \
		"$(WORLDSERVER_REALM_ID)" "$(WORLDSERVER_NAME)" "$(WORLDSERVER_IP)" "$(WORLDSERVER_PORT)" > "$@"

build:
ifneq ($(strip $(TC_GIT_BRANCH)),3.3.5)
ifeq ($(strip $(VARIANT)),npcbots)
	@echo "Error: The VARIANT '$(VARIANT)' can only be used with the TC_GIT_BRANCH '3.3.5' branch of TrinityCore; aborting!"
	@exit 1
endif
endif
	$(DOCKER_BUILD_CMD) \
 	-f $(DOCKERFILE) $(MAKEFILE_DIR) --target $(VARIANT) $(addprefix -t ,$(IMAGE_NAMES)) \
	--build-arg TC_GIT_BRANCH="$(TC_GIT_BRANCH)" \
	--build-arg TC_GIT_REPO="$(TC_GIT_REPO)" \
	--build-arg TC_VCS_REF="$(TC_VCS_REF)" \
	--build-arg VCS_REF="$(VCS_REF)" \
	--build-arg BUILD_DATE="$(BUILD_DATE)" \
	--build-arg BUILD_VERSION="$(BUILD_VERSION)" \
	--build-arg TDB_FULL_URL="$(TDB_FULL_URL)" \
	--build-arg MAPDATA_FULL_URL="$(MAPDATA_FULL_URL)" \
	--build-arg TRINITY_BOTS_REPO="$(TRINITY_BOTS_REPO)"
ifneq ($(strip $(DOCKER_BUILDX_PLATFORMS)),)
	docker buildx imagetools inspect $(IMAGE_NAME)
endif
	docker inspect $(IMAGE_NAME) | jq -r '.[0].Config.Labels'

ifeq ($(strip $(TC_GIT_BRANCH)),3.3.5)
build-npcbots: VARIANT=npcbots
build-npcbots: build
else
build-npcbots:
	@echo "Error: TC_GIT_BRANCH '$(TC_GIT_BRANCH)' used with target '$@' - Trinity-Bots (NPCBots) is only compatible with the TC_GIT_BRANCH '3.3.5' branch of TrinityCore; aborting!"
	exit 1
endif

build-all:
	$(MAKE) -f $(MAKEFILE) build TC_GIT_BRANCH=3.3.5 VARIANT=sql
	$(MAKE) -f $(MAKEFILE) build TC_GIT_BRANCH=3.3.5 VARIANT=slim
	$(MAKE) -f $(MAKEFILE) build TC_GIT_BRANCH=3.3.5 VARIANT=npcbots
	$(MAKE) -f $(MAKEFILE) build TC_GIT_BRANCH=cata_classic VARIANT=sql
	$(MAKE) -f $(MAKEFILE) build TC_GIT_BRANCH=cata_classic VARIANT=slim
	$(MAKE) -f $(MAKEFILE) build TC_GIT_BRANCH=master VARIANT=sql
	$(MAKE) -f $(MAKEFILE) build TC_GIT_BRANCH=master VARIANT=slim

pull:
	docker pull $(IMAGE_NAME)

pull-all:
	$(MAKE) -f $(MAKEFILE) pull TC_GIT_BRANCH=3.3.5 VARIANT=sql
	$(MAKE) -f $(MAKEFILE) pull TC_GIT_BRANCH=3.3.5 VARIANT=slim
	$(MAKE) -f $(MAKEFILE) pull TC_GIT_BRANCH=3.3.5 VARIANT=npcbots
	$(MAKE) -f $(MAKEFILE) pull TC_GIT_BRANCH=cata_classic VARIANT=sql
	$(MAKE) -f $(MAKEFILE) pull TC_GIT_BRANCH=cata_classic VARIANT=slim
	$(MAKE) -f $(MAKEFILE) pull TC_GIT_BRANCH=master VARIANT=sql
	$(MAKE) -f $(MAKEFILE) pull TC_GIT_BRANCH=master VARIANT=slim

ifneq ($(strip $(DOCKER_BUILDX_PLATFORMS)),)
define pushImage
	docker tag "$(1)" "$(2)"
	docker push "$(2)"
	skopeo copy --all "docker://$(1)" "docker://$(2)"
endef
else
define pushImage
	docker tag "$(1)" $(2)
	docker push "$(2)"
endef
endif

publish:
	$(foreach tag,$(filter-out $(IMAGE_NAME),$(IMAGE_NAMES)),$(call pushImage,$(IMAGE_NAME),$(tag)))
	$(call pushImage,$(IMAGE_NAME),$(IMAGE_REPO):$(TC_GIT_BRANCH)-$(VARIANT)-$(IMAGE_TC_VCS_REF))
ifeq ($(strip $(VARIANT)),sql)
	$(call pushImage,$(IMAGE_NAME),$(IMAGE_REPO):$(TC_GIT_BRANCH)-$(IMAGE_TC_VCS_REF))
endif
ifeq ($(strip $(TC_GIT_BRANCH)),3.3.5)
	$(call pushImage,$(IMAGE_NAME),$(IMAGE_REPO):$(VARIANT)-$(IMAGE_TC_VCS_REF))
ifeq ($(strip $(VARIANT)),sql)
	$(call pushImage,$(IMAGE_NAME),$(IMAGE_REPO):$(IMAGE_TC_VCS_REF))
endif
endif

publish-all:
	$(MAKE) -f $(MAKEFILE) publish TC_GIT_BRANCH=master VARIANT=slim
	$(MAKE) -f $(MAKEFILE) publish TC_GIT_BRANCH=master VARIANT=sql
	$(MAKE) -f $(MAKEFILE) publish TC_GIT_BRANCH=cata_classic VARIANT=slim
	$(MAKE) -f $(MAKEFILE) publish TC_GIT_BRANCH=cata_classic VARIANT=sql
	$(MAKE) -f $(MAKEFILE) publish TC_GIT_BRANCH=3.3.5 VARIANT=slim
	$(MAKE) -f $(MAKEFILE) publish TC_GIT_BRANCH=3.3.5 VARIANT=npcbots
	$(MAKE) -f $(MAKEFILE) publish TC_GIT_BRANCH=3.3.5 VARIANT=sql
	$(call pushImage,$(IMAGE_NAME),$(IMAGE_REPO):latest)

release-all:
	$(MAKE) -f $(MAKEFILE) build
	$(MAKE) -f $(MAKEFILE) test
	$(MAKE) -f $(MAKEFILE) build-all DOCKER_BUILDX_PLATFORMS=linux/amd64,linux/arm64
	$(MAKE) -f $(MAKEFILE) publish-all DOCKER_BUILDX_PLATFORMS=linux/amd64,linux/arm64

shell:
	docker run --rm -it $(IMAGE_NAME)

test:
ifeq ($(strip $(TC_GIT_BRANCH)),3.3.5)
	tests/docker-compose.sh
else
	@echo "Error: TC_GIT_BRANCH '$(TC_GIT_BRANCH)' used with target '$@' - $(DOCKER_COMPOSE_FILE) is only compatible with the TC_GIT_BRANCH '3.3.5' branch of TrinityCore; aborting!"
	exit 1
endif

# Explicitly remind the user where to copy their World of Warcraft game client
# files if they are missing.
%.MPQ:
	$(error Missing $@ necessary to generate worldserver map data; please copy \
		your World of Warcraft game client in to the $(GAME_CLIENT_DIR) directory)

# Generate worldserver map data from World of Warcraft game client data inside a
# Docker container.
mapdata: $(MAP_DATA_DIR)mmaps

# Download an archive of pre-generated worldserver map data.
MAPDATA_FILES_COUNT = $(shell find mapdata 2>/dev/null | wc -l)
MAPDATA_SIZE = $(shell du -s mapdata 2>/dev/null | cut -f1)
getmapdata:
ifeq ($(shell [ $(MAPDATA_FILES_COUNT) -gt 2900 ] && [ $(MAPDATA_SIZE) -gt 9000000 ] && echo true), true)
	@echo "make: Nothing to be done for 'getmapdata'."
else
	mkdir -pv ./mapdata
	cd ./mapdata && $(MAKEFILE_DIR)/getmapdata "$(TC_GIT_BRANCH)"
endif

$(MAP_DATA_DIR)Cameras $(MAP_DATA_DIR)dbc $(MAP_DATA_DIR)maps: $(MPQ)
	docker run --rm -v $(abspath $(GAME_CLIENT_DIR)):/wow -v $(abspath $(MAP_DATA_DIR)):/mapdata -w /mapdata $(IMAGE_NAME) mapextractor -i /wow -o /mapdata -e 7 -f 0

$(MAP_DATA_DIR)Buildings: $(MPQ)
	docker run --rm -v $(abspath $(GAME_CLIENT_DIR)):/wow -v $(abspath $(MAP_DATA_DIR)):/mapdata -w /mapdata $(IMAGE_NAME) vmap4extractor -l -d /wow/Data

$(MAP_DATA_DIR)vmaps: $(MPQ) $(MAP_DATA_DIR)Buildings
	docker run --rm -v $(abspath $(GAME_CLIENT_DIR)):/wow -v $(abspath $(MAP_DATA_DIR)):/mapdata -w /mapdata $(IMAGE_NAME) vmap4assembler /mapdata/Buildings /mapdata/vmaps

$(MAP_DATA_DIR)mmaps: $(MPQ) $(MAP_DATA_DIR)maps $(MAP_DATA_DIR)vmaps
	docker run --rm -v $(abspath $(GAME_CLIENT_DIR)):/wow -v $(abspath $(MAP_DATA_DIR)):/mapdata -w /mapdata $(IMAGE_NAME) mmaps_generator

# Convenience database backup target.
MYSQL_USER = trinity
MYSQL_PWD = trinity
MYSQL_HOST = 127.0.0.1
MYSQL_TCP_PORT = 3306
MYSQLDUMP_CMD = MYSQL_PWD=$(MYSQL_PWD) mysqldump -h $(MYSQL_HOST) -P $(MYSQL_TCP_PORT) -u $(MYSQL_USER) --hex-blob --column-statistics=0

.PHONY: dumpdb
dumpdb: auth-realmlist.sql auth-account.sql auth.sql characters.sql db-auth.sql db-characters.sql
	@echo
	ls -lah $^
	@echo
	@echo "Use 'make db-world.sql' to to backup the world database (usually only necessary when explicitly customised)."
	@echo

.PHONY: auth.sql characters.sql hotfixes.sql
auth.sql characters.sql hotfixes.sql:
	$(MYSQLDUMP_CMD) --no-create-info --insert-ignore --compact --no-create-db --ignore-table=$(basename $@).build_info --ignore-table=$(basename $@).updates --ignore-table=$(basename $@).updates_include --ignore-table=$(basename $@).uptime --databases $(basename $@) > $@

.PHONY: auth-realmlist.sql auth-account.sql
auth-realmlist.sql auth-account.sql:
	$(MYSQLDUMP_CMD) --no-create-info --replace --compact --no-create-db auth $(shell echo $(basename $@) | sed 's/^auth-//') > $@

.PHONY: db-auth.sql db-world.sql db-characters.sql db-hotfixes.sql
db-auth.sql db-world.sql db-characters.sql db-hotfixes.sql:
	$(MYSQLDUMP_CMD) $(shell echo $(basename $@) | sed 's/^db-//') > $@
