#!/usr/bin/env bash

set -euo pipefail

mysqlDefaultsFile () {
  grep -Ei '^LoginDatabaseInfo[[:space:]]*=[[:space:]]*' "${1:-/etc/*server.conf}" \
    | head -n 1 \
    | cut -d= -f2 \
    | sed -e 's/^[[:space:]]*"//' -e 's/"[[:space:]]*$//' \
    | awk -F';' '{print "[client]\nhost=\"" $1 "\"\nport=\"" $2 "\"\nuser=\"" $3 "\"\npassword=\"" $4 "\"\ndatabase=\"" $5 "\"\n"}'
}

main () {
  while ! mysql --defaults-file=<(mysqlDefaultsFile "${1:-}") --connect-timeout=5 --verbose --execute "SELECT VERSION(); SHOW TABLES;"
  do
    sleep 5
  done
}

main "$@"
