#!/usr/bin/env bash

set -Eeuo pipefail

# shellcheck disable=SC2154
trap 'declare rc=$?;
      >&2 echo "Unexpected error (exit-code $rc) executing $BASH_COMMAND at ${BASH_SOURCE[0]} line $LINENO";
      exit $rc' ERR

main () {
  declare -a scripts=()
  if [[ $# -ge 1 ]]
  then
    scripts=("$@")
  else
    mapfile -t scripts < <(find . -type f -size -40k \( ! -regex '.*/\..*' \) -print0 \
      | xargs -0 file \
      | grep -E '(sh|shell) script' \
      | cut -d: -f1 \
      | cut -b3- \
    )
  fi

  if [[ "${#scripts[@]}" -eq 0 ]]; then
      >&2 echo "No shell scripts found"
      return 0
  fi

  shellcheck -Calways "${scripts[@]}" || true

  shellcheck -a -f checkstyle "${scripts[@]}" \
    | xmlstarlet tr "$(dirname "${BASH_SOURCE[0]}")/checkstyle2junit.xslt" > junit.xml
}

main "$@"
