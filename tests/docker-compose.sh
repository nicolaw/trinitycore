#!/bin/sh
# MIT License
# Copyright (c) 2017-2024 Nicola Worthington <nicolaw@tfb.net>
# https://gitlab.com/nicolaw/trinitycore

# TODO: Test that we can then talk to the worldserver SOAP API okay.

set -e

# Try to find docker-compose command
DOCKER_COMPOSE_CMD="docker-compose"
if ! which docker-compose 2>/dev/null && docker compose version >/dev/null 2>&1
then
  DOCKER_COMPOSE_CMD="docker compose"
fi

healthyContainers () {
  case $DOCKER_COMPOSE_CMD in
    "docker-compose")
      $DOCKER_COMPOSE_CMD ps \
        | grep -c -E '^(authserver|worldserver)[[:space:]].*[[:space:]]Up[[:space:]].*\((healthy|health: starting)\)[[:space:]]'
      ;;
    "docker compose")
      # docker compose ps JSON output format is broken.
      $DOCKER_COMPOSE_CMD ps --format json \
        | jq -sc '.[] | if type=="array" then .[] else . end' \
        | jq . -s \
        | jq -r '.[]|[.Name, .State, .Health]|join(" ")' \
        | grep -c -E '^(authserver|worldserver)[[:space:]]running[[:space:]](healthy|starting)$'
      ;;
    *)
      >&2 echo "Error; do not know how to parse '$DOCKER_COMPOSE_CMD ps' output!"
      ;;
    esac
}

containersStarted () {
  docker logs authserver 2>&1 | grep -E '^(INFO  )?Added realm ".*" at .*\.' \
    && docker logs worldserver 2>&1 | grep -E '^(INFO  )?TrinityCore rev. .* \(worldserver-daemon\) ready\.\.\.'
}

cleanUp () {
  rm -f -- mysql.log worldserver.log authserver.log

  for action in stop kill
  do
    $DOCKER_COMPOSE_CMD -f ../docker-compose.yaml "$action" || true
    sleep 5
  done
  $DOCKER_COMPOSE_CMD -f ../docker-compose.yaml rm -f || true
  sleep 5
}

main () {
  if [ -e tests/docker-compose.sh ] && [ -e docker-compose.yaml ]
  then
    cd ./tests/
  fi

  cleanUp

  startTime="$(date +%s)"
  graceTime=$((startTime + 240))
  deadlineTime=$((startTime + 420))
  npcBotsSqlApplied=0

  $DOCKER_COMPOSE_CMD -f ../docker-compose.yaml up --detach

  while [ "$(date +%s)" -lt "$deadlineTime" ]
  do
    sleep 10

    $DOCKER_COMPOSE_CMD ps
    docker logs mysql       2>&1 | tee mysql.log       | tail | sed -e 's/^/mysql\t/'
    docker logs worldserver 2>&1 | tee worldserver.log | tail | sed -e 's/^/worldserver\t/'
    docker logs authserver  2>&1 | tee authserver.log  | tail | sed -e 's/^/authserver\t/'
    echo ""

    if   [ "$VARIANT" = "npcbots" ] \
      && [ "$npcBotsSqlApplied" -eq 0 ] \
      && docker logs worldserver 2>&1 | grep characters_npcbot_logs
    then
      docker run --rm --network trinitycore_default "nicolaw/trinitycore:3.3.5-npcbots" npcbots-install-sql
      npcBotsSqlApplied=1
    fi

    if [ "$(date +%s)" -ge "$graceTime" ] && [ "$(healthyContainers)" -lt 2 ] ; then
      >&2 echo "Too few healthy containers; aborting."
      break
    elif containersStarted ; then
      >&2 echo "Authserver and worldserver reporting as started."
      break
    fi
  done

  rc=0
  if [ "$(healthyContainers)" -lt 2 ] ; then
    rc=1
  fi
  containersStarted || rc=$?

  $DOCKER_COMPOSE_CMD -f ../docker-compose.yaml stop

  if [ $rc -eq 0 ] ; then
    >&2 echo "Passed."
  else
    >&2 echo "Failed."
  fi

  exit $rc
}

main "$@"

