-- http://www.ac-web.org/forums/archive/index.php/t-180196.html
-- https://wowdev.wiki/DB/ScalingStatValues
-- https://www.reddit.com/r/wow/comments/57thn8/class_stat_priorities_and_scaling/

-- The Sword of Azeroth
-- INSERT INTO item_template (entry, name, description, class, subclass, displayid, Quality, bonding, RequiredLevel, maxcount, AllowableClass, AllowableRace, BuyPrice, SellPrice, InventoryType, Material, sheath, Flags, BuyCount, stackable, ContainerSlots, dmg_min1, dmg_max1, dmg_type1, delay, MaxDurability, ammo_type, armor, block, BagFamily, socketBonus, FlagsExtra, StatsCount, socketColor_1, socketContent_1, socketColor_2, socketContent_2, socketColor_3, socketContent_3, stat_type1, stat_value1, stat_type2, stat_value2, stat_type3, stat_value3, Holy_res, Fire_res, Nature_res, Frost_res, Shadow_res, Arcane_res) VALUES (56808, 'The Sword of Azeroth', 'Blade of the Swordmaster!', 2, 15, 20195, 5, 1, 1, 1, -1, -1, 0, 0, 13, 0, 0, 0, 1, 1, 0, 225, 460, 0, 840, 180, 0, 0, 0, 0, 3302, 0, 3, 1, 1, 2, 1, 4, 1, 7, 280, 35, 265, 13, 203, 0, 0, 0, 0, 0, 0);

-- Needle
--DELETE FROM item_template WHERE entry = 13034; 
--INSERT INTO item_template (entry, NAME, description, class, subclass, displayid, Quality, bonding, RequiredLevel, ItemLevel, maxcount, AllowableClass, AllowableRace, BuyPrice, SellPrice, InventoryType, Material, sheath, Flags, BuyCount, stackable, ContainerSlots, dmg_min1, dmg_max1, dmg_type1, delay, MaxDurability, ammo_type, armor, block, BagFamily, socketBonus, FlagsExtra, StatsCount, socketColor_1, socketContent_1, socketColor_2, socketContent_2, socketColor_3, socketContent_3, stat_type1, stat_value1, stat_type2, stat_value2, stat_type3, stat_value3, stat_type4, stat_value4, Holy_res, Fire_res, Nature_res, Frost_res, Shadow_res, Arcane_res, ScalingStatDistribution, ScalingStatValue, spellid_1, spelltrigger_1, spellcharges_1, spellppmRate_1, spellcooldown_1, spellcategory_1, spellcategorycooldown_1, spellid_2, spelltrigger_2, spellcharges_2, spellppmRate_2, spellcooldown_2, spellcategory_2, spellcategorycooldown_2)
--VALUES (13034, 'Needle', 'Stick \'em with the pointy end', 2, 7, 28708, 5, 1, 15, 1, 1, - 1, - 1, 0, 0, 13, 0, 0, 8912904, 1, 1, 0, 280, 445, 0, 1100, 0, 0, 0, 0, 0, 3303, 768, 4, 2, 1, 2, 1, 2, 1, 7, 100, 5, 100, 6, 100, 31, 100, 0, 5, 0, 25, 0, 0, 6, 8+512+32768, 59830, 2, 0, 0, -1, 0, -1, 7597, 1, 0, 0, -1, 0, -1);

CREATE TEMPORARY TABLE IF NOT EXISTS starting_area_npc_vendor_items AS (
  SELECT
      entry,
      ROUND((RAND() * (5-1))+1) AS maxcount,        -- Adding maxcount column with random values
      ROUND((RAND() * (86400-900))+1) AS incrtime   -- Adding incrtime column with random values between 15 minutes (900 seconds) and 1 day (86400 seconds)
    FROM item_template
    WHERE (
            -- Heirloom items that scale with the player
            ScalingStatDistribution > 0 AND name NOT LIKE "Test %"
          )
          OR
          (
            -- Huge bags
            class = 1 AND ContainerSlots >= 36
          )
          OR
          entry IN (
            -- TGC Loot Cards (https://wowpedia.fandom.com/wiki/Loot_cards and https://www.wowhead.com/guide/vanity-collections/obtaining-trading-card-loot)
            33225, -- Reins of the Swift Spectral Tiger
            33224, -- Reins of the Spectral Tiger
            35225, -- X-51 Nether-Rocket
            35226, -- X-51 Nether-Rocket X-TREME
            38576, -- Big Battle Bear
            49290, -- Magic Rooster Egg
            23716 -- Carved Ogre Idol
          )
          OR
          name IN (
            -- TGC Loot Cards (https://wowpedia.fandom.com/wiki/Loot_cards and https://www.wowhead.com/guide/vanity-collections/obtaining-trading-card-loot)
            "Tabard of Flame",
            "Tabard of Frost",
            "Tabard of the Defender",
            "Tabard of Brilliance",
            "Tabard of Nature",
            "Tabard of Fury",
            "Tabard of the Void",
            "Tabard of the Arcane",
            "Hippogryph Hatchling",
            "Riding Turtle",
            "Picnic Basket",
            "Fishing Chair",
            "Banana Charm",
            "Imp in a Ball",
            "Goblin Gumbo Kettle",
            "Spectral Tiger",
            "Rocket Chicken",
            "Paper Flying Machine Kit",
            "Dragon Kite",
            "Papa Hummel's Old-Fashioned Pet Biscuit",
            "Goblin Weather Machine - Prototype 01-B",
            "Path of Illidan",
            "D.I.S.C.O.",
            "Soul-Trader Beacon",
            "Party G.R.E.N.A.D.E.",
            "The Flag of Ownership",
            "Sandbox Tiger",
            "Epic Purple Shirt",
            "Foam Sword Rack",
            "Path of Cenarius",
            "Ogre Pinata",
            "Blazing Hippogryph",
            "Landro's Gift Box",
            "Landro's Pet Box",
            "Instant Statue Pedestal",
            "Wooly White Rhino",
            "Ethereal Portal",
            "Paint Bomb",
            "Little White Stallion Bridle", -- Alliance races only
            "Little Ivory Raptor Whistle", -- Horde races only

            -- Collector's edition, Blizzcon rewards and Blizzard store
            "White Murloc Egg",
            "Pink Murloc Egg",
            "Orange Murloc Egg",
            "Blue Murloc Egg",
            "Heavy Murloc Egg",
            "Lurky's Egg",
            "Lil' Phylactery",
            "Lil' XT",
            "Murloc Costume",
            "Big Blizzard Bear",
            "Zergling Leash",
            "Panda Collar",
            "Pandaren Monk",
            "Diablo Stone",
            "Frosty's Collar",
            "Netherwhelp's Collar"
          )
);

-- Helper function to apply updates and inserts if conditions are met
DROP PROCEDURE IF EXISTS UpdateNpcVendor;

DELIMITER //

CREATE PROCEDURE UpdateNpcVendor(IN npcName VARCHAR(255), IN npcEntry INT(10) UNSIGNED)
UpdateNpcVendor: BEGIN
    
    -- Fetch npcEntry and npcflag and check if update is needed
    IF npcEntry > 0 THEN
        SELECT entry, name, npcflag INTO @npcEntry, @npcName, @npcFlag FROM creature_template WHERE name = npcName AND entry = npcEntry;
    ELSE
        SELECT entry, name, npcflag INTO @npcEntry, @npcName, @npcFlag FROM creature_template WHERE name = npcName LIMIT 1;
    END IF;

    -- Update npcflag if needed
    IF @npcFlag < 4224 THEN
        UPDATE creature_template SET npcflag = @npcFlag + 4096 + 128 WHERE entry = @npcEntry;
    END IF;

    SELECT COUNT(*) INTO @vendorCount FROM npc_vendor WHERE entry = @npcEntry;
    SELECT COUNT(*) INTO @tempTableCount FROM starting_area_npc_vendor_items;

    IF @vendorCount < @tempTableCount THEN
        INSERT INTO npc_vendor (entry, item, maxcount, incrtime)
        SELECT @npcEntry, entry, maxcount, incrtime FROM starting_area_npc_vendor_items;
    END IF;
END //

DELIMITER ;

-- Coldridge Valley (Gnome & Dwarf)
CALL UpdateNpcVendor('Sten Stoutarm', 0);

-- Northshire Abbey (Human)
CALL UpdateNpcVendor('Deputy Willem', 0);

-- Conservator Ilthalaine (Night Elf)
CALL UpdateNpcVendor('Conservator Ilthalaine', 0);

-- Megelon (Draenei)
CALL UpdateNpcVendor('Megelon', 0);

-- Kaltunk (Orc & Troll)
CALL UpdateNpcVendor('Kaltunk', 0);

-- Undertaker Mordo (Undead)
CALL UpdateNpcVendor('Undertaker Mordo', 0);

-- The Lich King (Death Knight)
CALL UpdateNpcVendor('The Lich King', 25462);

-- Grull Hawkwind (Tauren)
CALL UpdateNpcVendor('Grull Hawkwind', 0);

-- Magistrix Erona (Blood Elf)
CALL UpdateNpcVendor('Magistrix Erona', 0);

-- Validate npc_vendor table has been updated
--SELECT v.entry, c.name, v.item, i.name, v.maxcount, v.incrtime
--  FROM npc_vendor AS v
--  LEFT JOIN creature_template AS c ON v.entry = c.entry
--  LEFT JOIN item_template AS i ON i.entry = v.item
--  WHERE c.name IN ('Sten Stoutarm', 'Deputy Willem', 'Conservator Ilthalaine', 'Megelon', 'Kaltunk', 'Undertaker Mordo', 'Grull Hawkwind', 'Magistrix Erona') OR v.entry IN (25462);

SELECT v.entry, c.name, v.item, i.name, v.maxcount, v.incrtime
  FROM npc_vendor AS v
  LEFT JOIN creature_template AS c ON v.entry = c.entry
  LEFT JOIN item_template AS i ON i.entry = v.item
  WHERE c.name = 'Sten Stoutarm';

-- Validate creature_template table has been updated
SELECT entry, name, npcflag
  FROM creature_template
  WHERE name IN ('Sten Stoutarm', 'Deputy Willem', 'Conservator Ilthalaine', 'Megelon', 'Kaltunk', 'Undertaker Mordo', 'Grull Hawkwind', 'Magistrix Erona') OR entry IN (25462);
