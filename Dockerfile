# MIT License
# Copyright (c) 2017-2024 Nicola Worthington <nicolaw@tfb.net>
# https://gitlab.com/nicolaw/trinitycore/

# Intermediate build container can be pruned by listing filtered by image label:
# docker image rm "$(docker image ls --filter "label=org.opencontainers.image.title=nicolaw/trinitycore:intermediate" --quiet)"

FROM debian:bookworm-20240722-slim AS skeleton-source

LABEL org.opencontainers.image.authors="Nicola Worthington <nicolaw@tfb.net>" \
      org.opencontainers.image.title="nicolaw/trinitycore:intermediate"

RUN mkdir -pv /build/ /artifacts/ /src/

# https://github.com/TrinityCore/TrinityCore/blob/master/.circleci/config.yml
# https://github.com/TrinityCore/circle-ci
# https://trinitycore.atlassian.net/wiki/display/tc/Requirements
# https://trinitycore.info/en/install/requirements/linux
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get -qq -o Dpkg::Use-Pty=0 update \
 && apt-get -qq -o Dpkg::Use-Pty=0 install --no-install-recommends -y \
    binutils \
    ca-certificates \
    clang \
    cmake \
    curl \
    g++ \
    gcc \
    git \
    jq \
    libboost-all-dev \
    libbz2-dev \
    libmariadb-dev \
    libmariadb-dev-compat \
    libncurses-dev \
    libreadline-dev \
    libssl-dev \
    make \
    mariadb-client \
    patch \
    p7zip \
    tzdata \
    xml2 \
    zlib1g-dev \
 < /dev/null > /dev/null \
 && rm -rf /var/lib/apt/lists/* \
 && update-alternatives --install /usr/bin/cc cc /usr/bin/clang 100 \
 && update-alternatives --install /usr/bin/c++ c++ /usr/bin/clang 100

ARG TC_GIT_BRANCH=3.3.5
ARG TC_GIT_REPO=https://github.com/TrinityCore/TrinityCore.git
ARG TC_VCS_REF=
RUN git clone --branch "${TC_GIT_BRANCH}" --single-branch --depth 1 "${TC_GIT_REPO}" /src \
 && if [ "$(git -C /src rev-parse HEAD)" != "${TC_VCS_REF}" ] && [ -n "${TC_VCS_REF}"] ; then git -C /src checkout "${TC_VCS_REF}" ; fi



FROM skeleton-source AS trinitycore-build

WORKDIR /build

# https://trinitycore.info/en/install/Core-Installation/linux-core-installation
ARG INSTALL_PREFIX=/opt/trinitycore
ARG CONF_DIR=/etc
RUN cmake ../src -DTOOLS=1 -DWITH_WARNINGS=0 -DCMAKE_INSTALL_PREFIX="${INSTALL_PREFIX}" -DCONF_DIR="${CONF_DIR}" -Wno-dev \
 && make -j $(nproc) \
 && make install



FROM trinitycore-build AS trinitybots-build

WORKDIR /src

# https://github.com/trickerer/Trinity-Bots
ARG TRINITY_BOTS_REPO=https://github.com/trickerer/Trinity-Bots
RUN git clone --separate-git-dir="$(mktemp -u)" --branch master --single-branch --depth 1 "${TRINITY_BOTS_REPO}" ./Trinity-Bots \
 && patch -p1 < ./Trinity-Bots/NPCBots.patch

WORKDIR /build

ARG INSTALL_PREFIX=/opt/trinitycore
ARG CONF_DIR=/etc
RUN cmake ../src -DTOOLS=1 -DWITH_WARNINGS=0 -DCMAKE_INSTALL_PREFIX="${INSTALL_PREFIX}" -DCONF_DIR="${CONF_DIR}" -Wno-dev \
 && make -j $(nproc) \
 && make install



FROM trinitycore-build AS build-artifacts

WORKDIR /artifacts

# Install some additional utilitiy helper tools and reference material.
COPY ["gettdb","getsql","getmapdata","genmapdata","wait-for-mysql.sh","./${INSTALL_PREFIX}/bin/"]
COPY ["Dockerfile","docker-compose.yaml","aws/trinitycore-cfn.yaml","./${INSTALL_PREFIX}/"]
COPY ["LICENSE","*.md","./"]
ADD https://raw.githubusercontent.com/neechbear/tcadmin/master/tcadmin "./${INSTALL_PREFIX}/bin/tcadmin"
RUN mkdir -pv usr/bin/ && ln -s -t usr/bin/ /bin/env && chmod -v 0755 "./${INSTALL_PREFIX}/bin/"*
RUN mkdir -pv usr/share/git-core/templates/branches/ usr/share/git-core/templates/info/ usr/share/git-core/templates/

# Save upstream source Git SHA information that we built form.
ARG TDB_FULL_URL
ARG MAPDATA_FULL_URL
RUN git -C /src rev-parse HEAD > .git-rev \
 && git -C /src rev-parse --short HEAD > .git-rev-short \
 && echo "$TDB_FULL_URL" > .tdb-full-url \
 && echo "$MAPDATA_FULL_URL" > .mapdata-full-url \
 && dpkg-query -f '${binary:Package}\t${Version}\n' -W > .dpkg-packages \
 && openssl version > .openssl-version

# Copy binaries and example .dist.conf configuration files.
RUN tar -cf - \
    /build/revision_data.h \
    "${INSTALL_PREFIX}" \
    /bin/bash \
    /etc/ca-certificates* \
    /etc/*server.conf.dist \
    /etc/ssl/certs \
    /etc/timezone \
    /src/AUTHORS \
    /src/COPYING \
    /usr/bin/7zr \
    /usr/bin/curl \
    /usr/bin/git \
    /usr/bin/jq \
    /usr/bin/mariadb \
    /usr/bin/mysql \
    /usr/bin/stdbuf \
    /usr/bin/xml2 \
    /usr/lib/git-core/git-remote-http* \
    /usr/lib/p7zip/7zr \
    /usr/libexec/coreutils/libstdbuf.so \
    /usr/share/ca-certificates \
    /usr/share/zoneinfo \
  | tar -C /artifacts/ --transform='s,^build/,src/,' -xvf -

# Copy linked libraries and strip symbols from binaries.
# https://github.com/TrinityCore/TrinityCore/issues/30125
RUN ldd opt/trinitycore/bin/* usr/bin/* usr/lib/git-core/* > .ldd || true
RUN (ldd opt/trinitycore/bin/* usr/bin/* usr/lib/git-core/* | grep ' => ' | tr -s '[:blank:]' '\n' | grep '^/' | sort -u \
 && find /usr/lib/ -path '*/ossl-modules/legacy.so') | \
    xargs -I % sh -c 'mkdir -pv $(dirname .%); cp -v % .%'
RUN strip \
    "./${INSTALL_PREFIX}/bin/"*server \
    "./${INSTALL_PREFIX}/bin/"*extractor \
    "./${INSTALL_PREFIX}/bin/"*generator \
    "./${INSTALL_PREFIX}/bin/"*assembler

# Copy example .conf.dist configuration files into expected .conf locations.
RUN cp -v etc/worldserver.conf.dist etc/worldserver.conf \
 && find etc/ -name '*server.conf' -exec sed -i"" -r \
    -e 's,^(.*DatabaseInfo[[:space:]]*=[[:space:]]*")[[:alnum:]\.-]*(;.*"),\1mysql\2,' \
    -e 's,^(LogsDir[[:space:]]*=[[:space:]]).*,\1"/logs",' \
    -e 's,^(SourceDirectory[[:space:]]*=[[:space:]]).*,\1"/src",' \
    -e 's,^(MySQLExecutable[[:space:]]*=[[:space:]]).*,\1"/usr/bin/mysql",' \
    '{}' \; \
 && sed -i"" -r \
    -e 's,^(DataDir[[:space:]]*=[[:space:]]).*,\1"/mapdata",' \
    -e 's,^(Console\.Enable[[:space:]]*=[[:space:]]).*,\10,' \
    etc/worldserver.conf \
 && mkdir -pv "./${INSTALL_PREFIX}/etc/" \
 && ln -s -T /etc/worldserver.conf      "./${INSTALL_PREFIX}/etc/worldserver.conf" \
 && ln -s -T /etc/worldserver.conf.dist "./${INSTALL_PREFIX}/etc/worldserver.conf.dist"

RUN if ! ls -1 etc/authserver.conf.dist ; then exit 0 ; fi \
 && cp -v etc/authserver.conf.dist etc/authserver.conf \
 && ln -s -T /etc/authserver.conf       "./${INSTALL_PREFIX}/etc/authserver.conf" \
 && ln -s -T /etc/authserver.conf.dist  "./${INSTALL_PREFIX}/etc/authserver.conf.dist"

RUN if ! ls -1 etc/bnetserver.conf.dist ; then exit 0 ; fi \
 && cp -v etc/bnetserver.conf.dist etc/bnetserver.conf \
 && mv "./${INSTALL_PREFIX}/bin/"*.pem etc/ \
 && ln -s -T /etc/bnetserver.conf       "./${INSTALL_PREFIX}/etc/bnetserver.conf" \
 && ln -s -T /etc/bnetserver.conf.dist  "./${INSTALL_PREFIX}/etc/bnetserver.conf.dist" \
 && ln -s -T /etc/bnetserver.cert.pem   "./${INSTALL_PREFIX}/etc/bnetserver.cert.pem" \
 && ln -s -T /etc/bnetserver.key.pem    "./${INSTALL_PREFIX}/etc/bnetserver.key.pem" \
 && ln -s -T /etc/bnetserver.cert.pem   "./${INSTALL_PREFIX}/bin/bnetserver.cert.pem" \
 && ln -s -T /etc/bnetserver.key.pem    "./${INSTALL_PREFIX}/bin/bnetserver.key.pem"

# Copy SQL and revision data source files. (Later exclude old/ and updates/ on a "slim" image).
RUN tar -cf - /src/sql /src/revision_data.h | tar -C /artifacts/ -xvf - | tail

# Download TDB_full_world SQL dump to populate worldserver database.
# First try downloading without any arguments to try and read from $TC_CHROOT/.tdb-full-url,
# with a secondary fallback to explicitly querying with the SHA from /artifacts/.git-rev.
WORKDIR /artifacts/src/sql
RUN ls -al /artifacts \
 && (TC_CHROOT="/artifacts" "../../${INSTALL_PREFIX}/bin/gettdb" \
 || "../../${INSTALL_PREFIX}/bin/gettdb" "$(head -n 1 /artifacts/.git-rev | tr -cd 'a-f0-9')") \
 && rm -fv *.7z

# Convenience symlinks.
WORKDIR /artifacts
RUN for sql in src/sql/TDB_*.sql ; do ln -s "$sql" ; done
RUN ln -s src/sql/
RUN ln -s -T /src/sql/ "./${INSTALL_PREFIX}/sql"
RUN ln -s -T /src/ "./${INSTALL_PREFIX}/src"



FROM busybox:1.37.0-glibc AS busybox-build

# Copy built software artifcts into final image.
ARG INSTALL_PREFIX=/opt/trinitycore
COPY --from=build-artifacts /artifacts /
COPY --from=nicolaw/tcpasswd:latest /tcpasswd "${INSTALL_PREFIX}/bin/tcpasswd"

# Set suitible unprivileged user, home directory and initial working diretory.
ARG TRINITY_UID=1000
ARG TRINITY_GID=1000
RUN if ! id -g trinity ; then addgroup -g "${TRINITY_GID}" trinity ; fi
RUN if ! id -u trinity ; then adduser -G trinity -D -u "${TRINITY_UID}" -h "${INSTALL_PREFIX}" trinity ; fi



FROM scratch AS sql-build
COPY --from=busybox-build / /



FROM scratch AS slim-build
COPY --from=busybox-build / /
RUN rm -rf /src/sql/old /src/sql/updates /src/sql/TDB_full_world_* /TDB_full_world_*



FROM scratch AS npcbots-build
COPY --from=busybox-build / /
ARG INSTALL_PREFIX=/opt/trinitycore
COPY --from=trinitybots-build "${INSTALL_PREFIX}/bin/worldserver" "${INSTALL_PREFIX}/bin/worldserver"
COPY --from=trinitybots-build "/src/sql" "/src/sql"
COPY --from=trinitybots-build "/etc/*server.conf*" "/etc"
COPY --from=trinitybots-build "/src/Trinity-Bots" "/src/Trinity-Bots"
COPY ["npcbots-install-sql","./${INSTALL_PREFIX}/bin/"]
RUN cp /etc/worldserver.conf.dist /etc/worldserver.conf \
 && cp /etc/authserver.conf.dist /etc/authserver.conf \
 && sed -i"" -r \
    -e 's,^(.*DatabaseInfo[[:space:]]*=[[:space:]]*")[[:alnum:]\.-]*(;.*"),\1mysql\2,' \
    -e 's,^(LogsDir[[:space:]]*=[[:space:]]).*,\1"/logs",' \
    -e 's,^(SourceDirectory[[:space:]]*=[[:space:]]).*,\1"/src",' \
    -e 's,^(MySQLExecutable[[:space:]]*=[[:space:]]).*,\1"/usr/bin/mysql",' \
    /etc/worldserver.conf /etc/authserver.conf \
 && sed -i"" -r \
    -e 's,^(DataDir[[:space:]]*=[[:space:]]).*,\1"/mapdata",' \
    -e 's,^(Console\.Enable[[:space:]]*=[[:space:]]).*,\10,' \
    /etc/worldserver.conf
RUN cd /src/sql/Bots/ \
 && chmod +x *.sh \
 && ./merge_sqls_auth_unix.sh \
 && ./merge_sqls_characters_unix.sh \
 && ./merge_sqls_world_unix.sh



FROM scratch AS base-labels

ARG INSTALL_PREFIX=/opt/trinitycore
ENV LD_LIBRARY_PATH=/lib:/lib/x86_64-linux-gnu:/usr/lib/x86_64-linux-gnu:${INSTALL_PREFIX}/lib \
    PATH=/bin:/usr/bin:${INSTALL_PREFIX}/bin
USER trinity
WORKDIR /
CMD ["sh"]

ARG TC_GIT_BRANCH=3.3.5
ARG TC_VCS_REF
ARG BUILD_DATE
ARG VCS_REF
ARG BUILD_VERSION
ARG TDB_FULL_URL
ARG MAPDATA_FULL_URL

LABEL org.opencontainers.image.authors="Nicola Worthington <nicolaw@tfb.net>" \
      org.opencontainers.image.created="$BUILD_DATE" \
      org.opencontainers.image.title="nicolaw/trinitycore" \
      org.opencontainers.image.description="TrinityCore MMO World of Warcraft server" \
      org.opencontainers.image.documentation="https://gitlab.com/nicolaw/trinitycore/-/blob/main/README.md" \
      org.opencontainers.image.documentation2="https://gitlab.com/nicolaw/trinitycore/-/blob/main/GettingStarted.md" \
      org.opencontainers.image.tutorial="https://www.youtube.com/playlist?list=PLb_n3pHJuv5ZRI2QkdpnSS-2dPy6UPzSl" \
      org.opencontainers.image.url="https://nicolaw.uk/trinitycore" \
      org.opencontainers.image.source="https://gitlab.com/nicolaw/trinitycore/" \
      org.opencontainers.image.revision="$VCS_REF" \
      org.opencontainers.image.version="$BUILD_VERSION" \
      org.opencontainers.image.vendor="Nicola Worthington <nicolaw@tfb.net>" \
      org.opencontainers.image.licenses="MIT GPL-2.0" \
      org.opencontainers.image.mapdata-full-url="$MAPDATA_FULL_URL" \
      org.opencontainers.image.trinitycore.documentation="https://trinitycore.info/" \
      org.opencontainers.image.trinitycore.documentation2="https://trinitycore.atlassian.net/wiki/spaces/tc/overview" \
      org.opencontainers.image.trinitycore.url="https://www.trinitycore.org/" \
      org.opencontainers.image.trinitycore.source="https://github.com/TrinityCore/TrinityCore" \
      org.opencontainers.image.trinitycore.version="$TC_GIT_BRANCH" \
      org.opencontainers.image.trinitycore.revision="$TC_VCS_REF" \
      org.opencontainers.image.trinitycore.tdb-full-url="$TDB_FULL_URL"



FROM base-labels AS sql
ARG TC_GIT_BRANCH=3.3.5
COPY --from=sql-build / /
LABEL org.opencontainers.image.variant="sql" \
      org.opencontainers.image.docker.cmd.worldserver="docker run --rm -it -p 8085:8085 -p 3443:3443 -p 7878:7878 -v \$PWD/worldserver.conf:/etc/worldserver.conf -v \$PWD/mapdata:/mapdata -d nicolaw/trinitycore:${TC_GIT_BRANCH}-sql worldserver" \
      org.opencontainers.image.docker.cmd.authserver="docker run --rm -p 3724:3724 -p 1119:1119 -v \$PWD/authserver.conf:/etc/authserver.conf -d nicolaw/trinitycore:${TC_GIT_BRANCH}-sql authserver" \
      org.opencontainers.image.docker.cmd.bnetserver="docker run --rm -p 3724:3724 -p 1119:1119 -v \$PWD/bnetserver.conf:/etc/bnetserver.conf -d nicolaw/trinitycore:${TC_GIT_BRANCH}-sql bnetserver" \
      org.opencontainers.image.docker.cmd.genmapdata="docker run --rm -v \$PWD/World_of_Warcraft:/wow -v \$PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore:${TC_GIT_BRANCH}-sql genmapdata /wow /mapdata" \
      org.opencontainers.image.docker.cmd.mapextractor="docker run --rm -v \$PWD/World_of_Warcraft:/wow -v \$PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore:${TC_GIT_BRANCH}-sql mapextractor -i /wow -o /mapdata -e 7 -f 0" \
      org.opencontainers.image.docker.cmd.vmap4extractor="docker run --rm -v \$PWD/World_of_Warcraft:/wow -v \$PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore:${TC_GIT_BRANCH}-sql vmap4extractor -l -d /wow/Data" \
      org.opencontainers.image.docker.cmd.vmap4assembler="docker run --rm -v \$PWD/World_of_Warcraft:/wow -v \$PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore:${TC_GIT_BRANCH}-sql vmap4assembler /mapdata/Buildings /mapdata/vmaps" \
      org.opencontainers.image.docker.cmd.mmaps_generator="docker run --rm -v \$PWD/World_of_Warcraft:/wow -v \$PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore:${TC_GIT_BRANCH}-sql mmaps_generator" \
      org.opencontainers.image.docker.cmd.getmapdata="docker run --rm -v \$PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore:${TC_GIT_BRANCH}-sql getmapdata" \
      org.opencontainers.image.docker.cmd.gettdb="docker run --rm -v \$PWD:/src/sql -w /src/sql nicolaw/trinitycore:${TC_GIT_BRANCH}-sql gettdb" \
      org.opencontainers.image.docker.cmd.getsql="docker run --rm -v \$PWD:/src -w /src nicolaw/trinitycore:${TC_GIT_BRANCH}-sql getsql" \
      org.opencontainers.image.docker.cmd.tcpasswd="docker run --rm nicolaw/trinitycore:${TC_GIT_BRANCH}-sql tcpasswd" \
      org.opencontainers.image.docker-compose.cmd.tcpasswd="docker run --rm --network trinitycore_default nicolaw/trinitycore:${TC_GIT_BRANCH}-sql sh -c 'tcpasswd -username janedoe -password letmein -gmlevel 3 | MYSQL_PWD=trinity mysql -h mysql -D auth'"



FROM base-labels AS slim
ARG TC_GIT_BRANCH=3.3.5
COPY --from=slim-build / /
LABEL org.opencontainers.image.variant="slim" \
      org.opencontainers.image.docker.cmd.worldserver="docker run --rm -it -p 8085:8085 -p 3443:3443 -p 7878:7878 -v \$PWD/worldserver.conf:/etc/worldserver.conf -v \$PWD/mapdata:/mapdata -d nicolaw/trinitycore:${TC_GIT_BRANCH}-slim worldserver" \
      org.opencontainers.image.docker.cmd.authserver="docker run --rm -p 3724:3724 -p 1119:1119 -v \$PWD/authserver.conf:/etc/authserver.conf -d nicolaw/trinitycore:${TC_GIT_BRANCH}-slim authserver" \
      org.opencontainers.image.docker.cmd.bnetserver="docker run --rm -p 3724:3724 -p 1119:1119 -v \$PWD/bnetserver.conf:/etc/bnetserver.conf -d nicolaw/trinitycore:${TC_GIT_BRANCH}-slim bnetserver" \
      org.opencontainers.image.docker.cmd.genmapdata="docker run --rm -v \$PWD/World_of_Warcraft:/wow -v \$PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore:${TC_GIT_BRANCH}-slim genmapdata /wow /mapdata" \
      org.opencontainers.image.docker.cmd.mapextractor="docker run --rm -v \$PWD/World_of_Warcraft:/wow -v \$PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore:${TC_GIT_BRANCH}-slim mapextractor -i /wow -o /mapdata -e 7 -f 0" \
      org.opencontainers.image.docker.cmd.vmap4extractor="docker run --rm -v \$PWD/World_of_Warcraft:/wow -v \$PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore:${TC_GIT_BRANCH}-slim vmap4extractor -l -d /wow/Data" \
      org.opencontainers.image.docker.cmd.vmap4assembler="docker run --rm -v \$PWD/World_of_Warcraft:/wow -v \$PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore:${TC_GIT_BRANCH}-slim vmap4assembler /mapdata/Buildings /mapdata/vmaps" \
      org.opencontainers.image.docker.cmd.mmaps_generator="docker run --rm -v \$PWD/World_of_Warcraft:/wow -v \$PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore:${TC_GIT_BRANCH}-slim mmaps_generator" \
      org.opencontainers.image.docker.cmd.getmapdata="docker run --rm -v \$PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore:${TC_GIT_BRANCH}-slim getmapdata" \
      org.opencontainers.image.docker.cmd.gettdb="docker run --rm -v \$PWD:/src/sql -w /src/sql nicolaw/trinitycore:${TC_GIT_BRANCH}-slim gettdb" \
      org.opencontainers.image.docker.cmd.getsql="docker run --rm -v \$PWD:/src -w /src nicolaw/trinitycore:${TC_GIT_BRANCH}-slim getsql" \
      org.opencontainers.image.docker.cmd.tcpasswd="docker run --rm nicolaw/trinitycore:${TC_GIT_BRANCH}-slim tcpasswd" \
      org.opencontainers.image.docker-compose.cmd.tcpasswd="docker run --rm --network trinitycore_default nicolaw/trinitycore:${TC_GIT_BRANCH}-slim sh -c 'tcpasswd -username janedoe -password letmein -gmlevel 3 | MYSQL_PWD=trinity mysql -h mysql -D auth'"



FROM base-labels AS npcbots
ARG TC_GIT_BRANCH=3.3.5
COPY --from=npcbots-build / /
LABEL org.opencontainers.image.variant="npcbots" \
    org.opencontainers.image.docker.cmd.worldserver="docker run --rm -it -p 8085:8085 -p 3443:3443 -p 7878:7878 -v \$PWD/worldserver.conf:/etc/worldserver.conf -v \$PWD/mapdata:/mapdata -d nicolaw/trinitycore:${TC_GIT_BRANCH}-npcbots worldserver" \
    org.opencontainers.image.docker.cmd.authserver="docker run --rm -p 3724:3724 -p 1119:1119 -v \$PWD/authserver.conf:/etc/authserver.conf -d nicolaw/trinitycore:${TC_GIT_BRANCH}-npcbots authserver" \
    org.opencontainers.image.docker.cmd.genmapdata="docker run --rm -v \$PWD/World_of_Warcraft:/wow -v \$PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore:${TC_GIT_BRANCH}-npcbots genmapdata /wow /mapdata" \
    org.opencontainers.image.docker.cmd.mapextractor="docker run --rm -v \$PWD/World_of_Warcraft:/wow -v \$PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore:${TC_GIT_BRANCH}-npcbots mapextractor -i /wow -o /mapdata -e 7 -f 0" \
    org.opencontainers.image.docker.cmd.vmap4extractor="docker run --rm -v \$PWD/World_of_Warcraft:/wow -v \$PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore:${TC_GIT_BRANCH}-npcbots vmap4extractor -l -d /wow/Data" \
    org.opencontainers.image.docker.cmd.vmap4assembler="docker run --rm -v \$PWD/World_of_Warcraft:/wow -v \$PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore:${TC_GIT_BRANCH}-npcbots vmap4assembler /mapdata/Buildings /mapdata/vmaps" \
    org.opencontainers.image.docker.cmd.mmaps_generator="docker run --rm -v \$PWD/World_of_Warcraft:/wow -v \$PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore:${TC_GIT_BRANCH}-npcbots mmaps_generator" \
    org.opencontainers.image.docker.cmd.getmapdata="docker run --rm -v \$PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore:${TC_GIT_BRANCH}-npcbots getmapdata" \
    org.opencontainers.image.docker.cmd.gettdb="docker run --rm -v \$PWD:/src/sql -w /src/sql nicolaw/trinitycore:${TC_GIT_BRANCH}-npcbots gettdb" \
    org.opencontainers.image.docker.cmd.getsql="docker run --rm -v \$PWD:/src -w /src nicolaw/trinitycore:${TC_GIT_BRANCH}-npcbots getsql" \
    org.opencontainers.image.docker.cmd.npcbots-install-sql="docker run --rm nicolaw/trinitycore:${TC_GIT_BRANCH}-npcbots npcbots-install-sql" \
    org.opencontainers.image.docker-compose.cmd.npcbots-install-sql="docker run --rm --network trinitycore_default nicolaw/trinitycore:${TC_GIT_BRANCH}-npcbots npcbots-install-sql" \
    org.opencontainers.image.docker.cmd.tcpasswd="docker run --rm nicolaw/trinitycore:${TC_GIT_BRANCH}-npcbots tcpasswd" \
    org.opencontainers.image.docker-compose.cmd.tcpasswd="docker run --rm --network trinitycore_default nicolaw/trinitycore:${TC_GIT_BRANCH}-npcbots sh -c 'tcpasswd -username janedoe -password letmein -gmlevel 3 | MYSQL_PWD=trinity mysql -h mysql -D auth'" \
    org.opencontainers.image.trinity-bots.documentation="https://github.com/trickerer/Trinity-Bots/blob/master/README.md" \
    org.opencontainers.image.trinity-bots.source="https://github.com/trickerer/Trinity-Bots"



# Ensure that sql is the default target.
FROM sql
RUN true
