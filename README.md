# TrinityCore

This is a slim container image for TrinityCore.

Multi-architecture `linux/amd64` and `linux/arm64` images for WotLK (3.3.5),
WotLK Classic, Cataclysm Classic and the mainline master branch of TrinityCore
are built frequently published to `docker.io/nicolaw/trinitycore`.

## Published DockerHub image tags

### 3.3.5 WotLK variants

* Full w/SQL:
  [`latest`](https://hub.docker.com/r/nicolaw/trinitycore/tags?page=&page_size=&ordering=&name=latest),
  [`3.3.5-sql`](https://hub.docker.com/r/nicolaw/trinitycore/tags?page=&page_size=&ordering=&name=3.3.5-sql),
  `3.3.5`, `sql`
* Minimal:
  [`3.3.5-slim`](https://hub.docker.com/r/nicolaw/trinitycore/tags?page=&page_size=&ordering=&name=3.3.5-slim),
  `slim`
* Full [Trinity-Bots/NPCBots](https://github.com/trickerer/Trinity-Bots):
  [`3.3.5-npcbots`](https://hub.docker.com/r/nicolaw/trinitycore/tags?page=&page_size=&ordering=&name=3.3.5-npcbots),
  `npcbots`

### Cataclysm Classic variants

* Full w/SQL:
  [`cata_classic-sql`](https://hub.docker.com/r/nicolaw/trinitycore/tags?page=&page_size=&ordering=&name=cata_classic-sql),
  `cata_classic`
* Minimal:
  [`cata_classic-slim`](https://hub.docker.com/r/nicolaw/trinitycore/tags?page=&page_size=&ordering=&name=cata_classic-slim)

### Master branch variants

* Full w/SQL:
  [`master-sql`](https://hub.docker.com/r/nicolaw/trinitycore/tags?page=&page_size=&ordering=&name=master-sql),
  `master`
* Minimal:
  [`master-slim`](https://hub.docker.com/r/nicolaw/trinitycore/tags?page=&page_size=&ordering=&name=master-slim)

## Links

* [Getting started guide](https://gitlab.com/nicolaw/trinitycore/-/blob/main/GettingStarted.md) - simple tutorial to running your own World of Warcraft WotLK TrinityCore server
* [YouTube platlist of tutorial videos](https://www.youtube.com/playlist?list=PLb_n3pHJuv5ZRI2QkdpnSS-2dPy6UPzSl)
* [Building the container image](https://gitlab.com/nicolaw/trinitycore/-/blob/main/Building.md) - guide to building the container image yourself
* [Docker Hub container image](https://hub.docker.com/r/nicolaw/trinitycore/) - multi-architecture x86_64 and ARM64 images
* [Dockerfile](https://gitlab.com/nicolaw/trinitycore/-/blob/main/Dockerfile)
* [AWS CloudFormation template](https://gitlab.com/nicolaw/trinitycore/-/tree/main/aws) - fully automated deployment into Amazon Web Services for the 3.3.5 WotLK branch only
* [docker-compose.yaml](https://gitlab.com/nicolaw/trinitycore/-/blob/main/docker-compose.yaml) - for the 3.3.5 WotLK branch only

Upstream projects:

* [TrinityCore GitHub project](https://github.com/TrinityCore/TrinityCore)
* [Trinity-Bots (NPCBots) GitHub project](https://github.com/trickerer/Trinity-Bots)

## Synopsis

    # Describe information about the container image.
    $ docker pull nicolaw/trinitycore
    $ docker inspect nicolaw/trinitycore | jq -r '.[0].Config.Labels'
    $ docker run --rm nicolaw/trinitycore sh -c "ls -lh /opt/trinitycore/* /etc/*server.conf* /usr/local/bin"

    # Download pre-generated map data using helper script.
    $ docker run --rm -v $PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore getmapdata

    # Alternatively extract map data using helper script.
    $ docker run --rm -v $PWD/World_of_Warcraft:/wow -v $PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore genmapdata /wow /mapdata
    
    # Alternatively Extract map data manually.
    $ docker run --rm -v $PWD/World_of_Warcraft:/wow -v $PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore mapextractor -i /wow -o /mapdata -e 7 -f 0
    $ docker run --rm -v $PWD/World_of_Warcraft:/wow -v $PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore vmap4extractor -l -d /wow/Data
    $ docker run --rm -v $PWD/World_of_Warcraft:/wow -v $PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore vmap4assembler /mapdata/Buildings /mapdata/vmaps
    $ docker run --rm -v $PWD/World_of_Warcraft:/wow -v $PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore mmaps_generator
    
    # Run authserver/bnetserver and worldserver in the background.
    $ docker run --name bnetserver -d --restart unless-stopped --rm -p 3724:3724 -p 1119:1119 -v $PWD/bnetserver.conf:/etc/bnetserver.conf nicolaw/trinitycore bnetserver
    $ docker run --name authserver -d --restart unless-stopped --rm -p 3724:3724 -p 1119:1119 -v $PWD/authserver.conf:/etc/authserver.conf nicolaw/trinitycore authserver
    $ docker run --name worldserver -d --restart unless-stopped --rm -it -p 8085:8085 -p 3443:3443 -p 7878:7878 -v $PWD/worldserver.conf:/etc/worldserver.conf -v $PWD/mapdata:/mapdata nicolaw/trinitycore worldserver

    # Optionally install Trinity-Bots (NPCBots) database SQL updates.
    # (Optionally add --network trinitycore_database or --network trinitycore_default as necessary).
    $ docker run --rm nicolaw/trinitycore npcbots-install-sql

## Running

### Authentication server

    docker run --rm -p 3724:3724 -v $PWD/authserver.conf:/etc/authserver.conf -d nicolaw/trinitycore authserver

### Height map data generation for world server

The following command assumes that your World of Warcraft 3.3.5 (WotLK) game
client is located in the `$PWD/World_of_Warcraft` directory on your local
machine.

    mkdir ./mapdata/
    chmod a+rwx ./mapdata/
    docker run --rm -v $PWD/World_of_Warcraft:/wow -v $PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore genmapdata /wow /mapdata

This process will take several hours to produce approximately 4 GB of data
inside the `mapdata` directory.

Alternatively, a pre-packaged archive of the world server height map data may be
downloaded from <https://gitlab.com/nicolaw/trinitycore/-/packages> using the
following command:

    docker run --rm -v $PWD/mapdata:/mapdata -w /mapdata nicolaw/trinitycore getmapdata

### World server

    docker run --rm -p 8085:8085 -p 3443:3443 -p 7878:7878 -v $PWD/worldserver.conf:/etc/worldserver.conf -v $PWD/mapdata:/mapdata -d -it nicolaw/trinitycore worldserver

### Docker compose

A convenience [docker-compose.yaml](https://gitlab.com/nicolaw/trinitycore/-/blob/main/docker-compose.yaml)
is available at <https://gitlab.com/nicolaw/trinitycore/-/blob/main/docker-compose.yaml> for easy operation of the
database, world server and auth server using `docker-compose`:

    docker-compose up

Refer to the Docker Compose documentation at <https://docs.docker.com/compose> for
more information.

## Configuration files and customisation

Default configuration files are in `/etc/authserver.conf` and `/etc/worldserver.conf`
inside the container image. These may be modified outside of the container and
mounted as volumes into the container to customise the operation of the
authentication and world servers:

    docker run --rm nicolaw/trinitycore cat /etc/authserver.conf > authserver.conf
    docker run --rm nicolaw/trinitycore cat /etc/worldserver.conf > worldserver.conf

See <https://trinitycore.atlassian.net/wiki/spaces/tc/overview> for more information.

## Account creation

Assuming that you started the world server container with an interactive TTY
(the `-it` argument to `docker run`), you may attach to the container to execute
to run interactive commands as documented by
<https://trinitycore.atlassian.net/wiki/spaces/tc/pages/2130065/gm+commands>.

    docker ps
    docker attach <container-ID|container-name>

Pressing `Control-P` then `Control-Q` will disconnect you from the container console.

Example:

    $ docker ps
    CONTAINER ID   IMAGE                        COMMAND                  CREATED        STATUS        PORTS     NAMES
    b897d9c319e5   nicolaw/trinitycore:latest   "worldserver -c /etc…"   23 hours ago   Up 23 hours             worldserver
    
    $ docker attach worldserver
    TC> .account create janedoe letmein jane.doe@example.com
    TC> INFO  CLI command under processing...
    Account created: janedoe
    TC> .account delete janedoe
    TC> INFO  CLI command under processing...
    Account deleted: JANEDOE
    TC> read escape sequence

### tcpasswd

The `tcpasswd` helper command from `docker.io/nicolaw/tcpasswd:latest` is
included in this container image. This provides a convenient means of
generating username and passwords that may be inserted into the TrinityCore
`auth` database.

    docker run --rm nicolaw/trinitycore tcpasswd --help
    docker run --rm nicolaw/trinitycore tcpasswd --username janedoe --password letmein

The SQL output from this command may be sent directly into MySQL:

    docker run --rm nicolaw/trinitycore \
      tcpasswd --username janedoe --password letmein --output sql | mysql -h localhost -u trinity -p -D auth

A similar example, entirely executed inside the Docker container, thus not
requiring the `mysql` client command to be present on the host machine:

    docker run --rm --network trinitycore_default nicolaw/trinitycore sh -c \
      "tcpasswd --username janedoe --password letmein --output sql | MYSQL_PWD=trinity mysql -h mysql -D auth"

Refer to <https://gitlab.com/nicolaw/tcpasswd/> and
<https://hub.docker.com/r/nicolaw/tcpasswd> for more information.
