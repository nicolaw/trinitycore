#!/usr/bin/env sh
# shellcheck disable=SC3043

# MIT License
# Copyright (c) 2024-2024 Nicola Worthington <nicolaw@tfb.net>
# https://gitlab.com/nicolaw/trinitycore

set -e

isDockerEnv() {
  [ -e /.dockerenv ]
}

isTCContainer() {
  isDockerEnv \
  && [ -e "${TC_CHROOT}/.git-rev" ] \
  && [ -e "${TC_CHROOT}/.git-rev-short" ] \
  && [ -e "${TC_CHROOT}/.tdb-full-url" ]
}

getDatabaseInfoLine () {
  local file="$1"
  local configKey="$2"
  # shellcheck disable=SC1087
  grep -Ei "^[[:space:]]*$configKey[[:space:]]*=[[:space:]]*" "$file" \
    | cut -d= -f2 \
    | sed 's/^[[:space:]\"]*//g; s/[[:space:]\"]$//g;'
}

setDatabaseCredentials () {
  local configKey="$1"

  local conf="worldserver.conf"
  if isTCContainer ; then
    conf="/etc/worldserver.conf"
  fi

  if ! [ -e "$conf" ] ; then
    >&2 "Unable to locate world sevrer configuration file '$conf' for database credentials; aborting!"
    exit 1
  fi
  if ! [ -r "$conf" ] ; then
    >&2 "Unable to read world sevrer configuration file '$conf' for database credentials; aborting!"
    exit 1
  fi

  local DbInfoLine=""
  DbInfoLine="$(getDatabaseInfoLine "$conf" "$configKey")"
  MYSQL_HOST="$(echo "$DbInfoLine" | cut -d";" -f1)"
  MYSQL_TCP_PORT="$(echo "$DbInfoLine" | cut -d";" -f2)"
  DB_USER="$(echo "$DbInfoLine" | cut -d";" -f3)"
  MYSQL_PWD="$(echo "$DbInfoLine" | cut -d";" -f4)"
  DB_NAME="$(echo "$DbInfoLine" | cut -d";" -f5)"
  export MYSQL_HOST MYSQL_TCP_PORT DB_USER MYSQL_PWD DB_NAME
}

printHelp() {
  echo "Usage: install-npcbots-sql [-h|--help] [-n|--dry-run]"

  echo ""
  echo "This command will install the database updates required to operate the"
  echo "Trinity-Bots NPCBots patch."
  echo ""
  echo "Refer to https://github.com/trickerer/Trinity-Bots for more information."
  echo ""
}

main () {
  local arg=""
  local mysqlCmd="mysql"

  for arg in "$@" ; do
    if [ "$arg" = "-h" ] || [ "$arg" = "--help" ] ; then
      printHelp >&2
      exit 0
    elif [ "$arg" = "-n" ] || [ "$arg" = "--dry-run" ] ; then
      mysqlCmd="echo mysql"
    fi
  done

  local sqlFile="ALL_auth.sql"
  if ! [ -e "$sqlFile" ] ; then
    sqlFile="/src/sql/Bots/ALL_auth.sql"
  fi
  setDatabaseCredentials LoginDatabaseInfo
  $mysqlCmd -u "$DB_USER" -P "$MYSQL_TCP_PORT" -h "$MYSQL_HOST" -D "$DB_NAME" < "$sqlFile"

  sqlFile="ALL_characters.sql"
  if ! [ -e "$sqlFile" ] ; then
    sqlFile="/src/sql/Bots/ALL_characters.sql"
  fi
  setDatabaseCredentials CharacterDatabaseInfo
  $mysqlCmd -u "$DB_USER" -P "$MYSQL_TCP_PORT" -h "$MYSQL_HOST" -D "$DB_NAME" < "$sqlFile"

  sqlFile="ALL_world.sql"
  if ! [ -e "$sqlFile" ] ; then
    sqlFile="/src/sql/Bots/ALL_world.sql"
  fi
  setDatabaseCredentials WorldDatabaseInfo
  $mysqlCmd -u "$DB_USER" -P "$MYSQL_TCP_PORT" -h "$MYSQL_HOST" -D "$DB_NAME" < "$sqlFile"
}

main "$@"
