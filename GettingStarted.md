# Getting started

This guide will walk you through setting up a TrinityCore private WoW server for
Wrath of the Lich King (game client version 3.3.5a).

A playlist of quick start tutorial videos can be found on YouTube at
https://www.youtube.com/playlist?list=PLb_n3pHJuv5ZRI2QkdpnSS-2dPy6UPzSl.

## Requirements

You will need a Linux, macOS or Windows running WSL2 machine that has the
following things installed:

  * `make`
  * `git`
  * `jq`
  * `docker` - Follow instructions at https://docs.docker.com/get-docker/
  * `docker-compose` - Follow instructions at https://docs.docker.com/compose/install/

Docker Compose is only required if you are using a particularly old
version of Docker that does not support the `docker compose` command.
Refer to https://docs.docker.com/compose/migrate/ for more details.

You will also need a *legitimate* copy of World of Warcraft: Wrath of the Lich
King (game client version 3.3.5a).

### Debian & Ubuntu

These required packages can be installed on Debian & Ubuntu by running the
following from your Linux shell:

    sudo apt-get install make git jq
    curl -sSL https://get.docker.com/ | sh

### CentOS & RedHat Enterprise Linux

These required packages can be installed on CentOS & RHEL by running the
following from your Linux shell:

    sudo yum install make git jq
    curl -sSL https://get.docker.com/ | sh

### macOS

These required packages can be installed using [Brew](https://brew.sh/) on
macOS by running the following from your terminal command line shell:

    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    brew install make git jq

[Docker Desktop on Mac](https://docs.docker.com/desktop/mac/install/) can be
installed from https://docs.docker.com/desktop/mac/install/.

## Installation

You will need to preform 4 distinct steps in order to start your private
TrinityCore server, (and start playing on it).

  1. Download _or_ build the TrinityCore container image.

  2. Generate the map data used by the `worldserver`. This will require a copy
     of the World of Warcraft game client files.

  3. Start the TrinityCore and database containers.

  4. Configure your World of Warcraft game client, then connect to your
     TrinityCore private WoW server.

### Downloading pre-built TrinityCore container

From your Linux or macOS shell, run the following:

    docker pull nicolaw/trinitycore

You can check how up-to-date the downloaded container image is by running the
following command:

    docker inspect nicolaw/trinitycore | jq -r '.[0].Config.Labels'

If you find that it is too old and that you need a newer version, you can build
your own version of the container by following the instructions in the next step
instead.

### Optionally building TrinityCore cotnainer

This step is optional and unnecessary if you download the pre-build TrinityCore
container image as described in the previous step.

From your Linux or macOS shell, run the following:

    git clone https://gitlab.com/nicolaw/trinitycore
    cd trinitycore
    make build

Depending on the performance of your machine, this may take up to 1 hour to
complete.

The TrinityCore conatiner image `nicolaw/trinitycore:3.3.5-sql` should now be
built and ready to use.

### Optionally building the Trinity-Bots (NPCBots) variant

If you wish to build the Trinity-Bots (NPCBots) variant of the server
(`nicolaw/trinitycore:3.3.5-npcbots`), then use this slightly different build
command:

    make build-npcbots

### World server height map data

The world server needs to access height map data in order to operate. This can
be obtained by downloading a pre-generated archive of the map data, or it can
be generated from the data files in the World of Warcraft game client.

To download a pre-generated archive, run the following:

    make getmapdata

If you prefer to generate your own height map data files, follow the
instructions in the next section.

#### Generating height map data (optional)

This process is not necessary if you have opted to download the pre-generated
archive as described in the previous step. This process needs to read the
data files from your copy of the World of Warcaft game client.

You should copy your game client (usually in `C:\Program Files
(x86)\World of Warcraft\` on Windows, or `/Applications/World of Warcraft.app`
on macOS), in to a directory called `World_of_Warcraft` (using underscores
instead of spaces), under the `trinitycore` directory that you created in the
previous compile steps.

If you are building this on a remote Linux machine, you may wish to use
something like WinSCP to copy the files. 

![Copying C:\Program Files (x86)\World of Warcraft\ to ~/trinitycore/World_of_Warcraft](.GettingStarted1.gif)

You can now run the next command to generate the map data.

    make mapdata

Depending on the performance of your machine, this may take up to 4 hours to
complete.

### Starting the TrinityCore server

You are now ready to start your TrinityCore server.

The first time you start your server, it will create and import data in to the
MariaDB database. This may take a couple of minutes.

To start the server, simply run the following:

    make run

To stop the server, press `Control-C`.

You can now stop and start your TrinityCore server whenever you wish. The server
may be run permanently as a detached background service by using
`docker-compose` directly:

    docker-compose stop
    docker-compose start

### Starting the Trinity-Bots (NPCBots) variant

If you wish to run the Trinity-Bots (NPCBots) variant of the server, then use
these slightly different commands:

    make run-npcbots
    
    VARIANT=npcbots WORLDSERVER=worldserver-npcbots.conf docker-compose start
    VARIANT=npcbots WORLDSERVER=worldserver-npcbots.conf docker-compose sttop

You may need to explicitly update the database with the Trinity-Bots (NPCBots)
SQL updates. This can be achieved by running the following command once after
you have already started the server:

    docker run --rm --network trinitycore_default nicolaw/trinitycore:3.3.5-npcbots npcbots-install-sql

If you have modified the `docker-compose.yaml` file to use a different network,
then make sure that you update the `--network trinitycore_default` argument to
match. For example, you may want to change it to `--network trinitycore_database`.

### Creating a GM administrator account

A default GM account username `trinity` and password `trinity` is automatically
created when using the `docker-compose.yaml`.

To create additional accounts you may attach to the container to execute to run
interactive commands as documented by
https://trinitycore.atlassian.net/wiki/spaces/tc/pages/2130065/gm+commands.

    docker attach worldserver

Pressing `Control-P` then `Control-Q` will disconnect you from the container console.

Example:

    $ docker attach worldserver
    TC> .account create janedoe letmein jane.doe@example.com
    TC> INFO  CLI command under processing...
    Account created: janedoe
    TC> .account set gmlevel janedoe 3 -1
    TC> INFO  CLI command under processing...
    Security level of account JANEDOE changed to 3.
    TC> read escape sequence

Alternatively you can use the following command line to insert new user
accounts directly into the database:

    $ docker run --rm --network trinitycore_default nicolaw/trinitycore sh -c "tcpasswd -username janedoe -password letmein -gmlevel 3 | MYSQL_PWD=trinity mysql -h mysql -D auth"

### Configuring your Game Client

As with any private WoW server, you will need to edit your `realmlist.wtf` file
in your `World of Warcraft\Data\enUS\` game client directory. Simply open the
file in your favorite text editor (or Notepad), and change the logon server to
be the IP address or hostname of the Linux machine that will be running your
TrinityCore server.

You can now launch your World of Warcraft game client, and login with the
default username `trinity` and password `trinity`.

The default `trinity` account has full Game-Master (GM) permissions. See
https://trinitycore.atlassian.net/wiki/display/tc/GM+Commands for a full list of
available commands.

Enjoy!
